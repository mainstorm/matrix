﻿namespace Matrixes
{
    public interface IDataProvider
    {
        void Save(Matrix matrix, string name);

        Matrix Load(string name);
    }
}

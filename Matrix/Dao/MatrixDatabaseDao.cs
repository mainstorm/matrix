﻿using Matrixes.Generators;
using Matrixes.Mappers;
using Matrixes.Saver;
using System;

namespace Matrixes
{
    internal sealed class MatrixDatabaseDao : IDataProvider
    {
        private readonly IDataBaseProvider _dataBaseProvider;
        private readonly DatabaseMatrixSaveQueryBuilder _saveQueryBuilder = new DatabaseMatrixSaveQueryBuilder();
        private readonly DatabaseMatrixLoadQueryBuilder _loadQueryBuilder = new DatabaseMatrixLoadQueryBuilder();

         public MatrixDatabaseDao(IDataBaseProvider dataProvider)
        {
            _dataBaseProvider = dataProvider ?? throw new ArgumentNullException(nameof(dataProvider));
            //_saveQueryBuilder = saveQueryBuilder ?? throw new ArgumentNullException(nameof(saveQueryBuilder));
            //_loadQueryBuilder = loadQueryBuilder ?? throw new ArgumentNullException(nameof(loadQueryBuilder));
        }

        public void Save(Matrix matrix, string name)
        {
            var matrixConvertor = new MatrixToListItemConvertor();
            var sql = _saveQueryBuilder.BuildQuery(matrixConvertor.ConvertToMatrixItems(matrix), name);
            _dataBaseProvider.ExecuteNonQuery(sql);
        }

        public Matrix Load(string name)
        {
            var matrixConvertor = new MatrixToListItemConvertor();
            var sql = _loadQueryBuilder.BuildQuery(name);
            var matrixItems = _dataBaseProvider.ExecuteQuery(sql, new MatrixItemMapper());
            return matrixConvertor.ConvertToMatirx(matrixItems);
        }
    }
}

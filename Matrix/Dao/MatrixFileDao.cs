﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrixes.Serilization;

namespace Matrixes
{
    class MatrixFileDao : IDataProvider
    {
        private ISerialize GetStateSerialize(string name)
        {
           if (name.Contains(".json"))
              return new Json(); 
           else 
              return new BinaryState();
        }
            
        public void Save(Matrix matrix, string name)
        {
            var stateSerialize = GetStateSerialize(name);
            FileProvider provider = new FileProvider(name);
            provider.Write(stateSerialize.Serialize(matrix));
        }

        public Matrix Load(string name)
        {
            var stateSerialize = GetStateSerialize(name);
            FileProvider provider = new FileProvider(name);
            var result = stateSerialize.DeSerialize(provider.Read());

            return result;
        }

    }
}

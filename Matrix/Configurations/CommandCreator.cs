﻿using Matrixes.Commands;
using System.Collections.Generic;
using System.Linq;

namespace Matrixes.Configurations
{
    internal sealed class CommandCreator
    {
        public IDictionary<CommandType, CommandDescription> CreateCommands()
        {
            var sharedCommandData = new SharedCommandData();
            var matrixGenerateCommand = new MatrixGenerateCommand(sharedCommandData);
            var printMatrixCommand = new PrintMatrixCommand(sharedCommandData);
            var saveToFileCommand = new SaveToFileCommand(sharedCommandData);
            var loadFromFileCommand = new LoadFromFileCommand(sharedCommandData);
            var commands = new[] {
                new CommandDescription
                {
                    Type = CommandType.Matrix,
                    Caption = $"Генерация матрицы 3 на 3",
                    ExecuteCommand = () => matrixGenerateCommand.Execute()
                },
                new CommandDescription
                {
                    Type = CommandType.PrintMatrix,
                    Caption = $"Вывести матрицу на экран",
                    ExecuteCommand = () => printMatrixCommand.Execute()
                },
                new CommandDescription
                {
                    Type = CommandType.SaveToFile,
                    Caption = $"Сохранить в файл",
                    ExecuteCommand = () => saveToFileCommand.Execute()
                },
                new CommandDescription
                {
                    Type = CommandType.LoadFromFile,
                    Caption = $"Загрузить из файла",
                    ExecuteCommand = () => loadFromFileCommand.Execute()
                },
                new CommandDescription
                {
                    Type = CommandType.Exit,
                    Caption = $"Выход",
                    ExecuteCommand = () => { }
                }
            };
            return commands.ToDictionary(c => c.Type);
        }
    }
}

﻿using Matrixes.Commands;
using System;

namespace Matrixes.Configurations
{
    public class CommandDescription
    {
        public CommandType Type { get; set; }

        public string Caption { get; set; } 

        public Action ExecuteCommand { get; set; }
    }
}

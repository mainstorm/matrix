﻿using Matrixes.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Matrixes
{
    class Program
    {
        static void Main(string[] args)
        {
            var commandCreator = new CommandCreator();
            var commands = commandCreator.CreateCommands();
            var commandCaptions = commands
                .OrderBy(c => c.Key)
                .Select(c => GetCaption(c.Value))
                .ToArray();
            
            //Matrix matrix = null;
            //IDataProvider data;
            CommandType pressedCommand;
            //var databaseConfig = new DatabaseConfiguration();
            do
            {
                Console.WriteLine(string.Join("\n", commandCaptions));
                pressedCommand = CommandProcessing(commands);

                //Console.WriteLine("1. Генерация матрицы 3 на 3");
                //Console.WriteLine("2. Запись матрицы в БД");
                //Console.WriteLine("3. Запись матрицы в файл");
                //Console.WriteLine("4. Загрузка матрицы из файла");
                //Console.WriteLine("5. Загрузка матрицы из БД");
                //commandType = (CommandType)Convert.ToInt32(Console.ReadLine());

                //switch (commandType)
                //{
                //    case CommandType.Matrix:
                //        var matrixGenerator = new MatrixGenerator();
                //        matrix = matrixGenerator.CreateRandomMatrix(3, 3);
                //        printMatrix(matrix);
                //        break;
                //    case CommandType.SaveToDatabase:

                //        if (matrix != null)
                //        {
                //            Console.WriteLine("Введите название таблицы в которую запишем матрицу");
                //            var provider = new DataBaseProvider(databaseConfig);  ///why?????(нельзя использовать dataProvider)
                //            data = new MatrixDatabaseDao(provider);
                //            data.Save(matrix, Console.ReadLine());
                //        } else Console.WriteLine("Нужно сгенерировать матрицу");

                //        break;
                //    case CommandType.SaveToFile:
                //        if (matrix != null)
                //        {
                //            Console.WriteLine("Введите название файла в которую запишем матрицу");
                //            data = new FileProvider();
                //            data.Save(matrix, Console.ReadLine());
                //        }
                //        else Console.WriteLine("Нужно сгенерировать матрицу");
                //        break;
                //    case CommandType.LoadFromFile:
                //        Console.WriteLine("Введите название файла");
                //        data = new FileProvider();
                //        matrix = data.Load(Console.ReadLine());
                //        printMatrix(matrix);
                //        break;
                //    case CommandType.LoadFromDatabase:
                //        Console.WriteLine("Введите название таблицы");
                //        var dataProvider = new DataBaseProvider(databaseConfig);
                //        data = new MatrixDatabaseDao(dataProvider);
                //        matrix = data.Load(Console.ReadLine());
                //        printMatrix(matrix);
                //        break;
                //}
            } while (pressedCommand != CommandType.Exit);



        }

        private static CommandType CommandProcessing(IDictionary<CommandType, CommandDescription> commands)
        {
            try
            {
                var commandType = (CommandType)Convert.ToInt32(Console.ReadLine());
                if (!commands.ContainsKey(commandType))
                {
                    throw new Exception("Команда не найдена");
                }
                commands[commandType].ExecuteCommand();
                return commandType;
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            return CommandType.Unknown;
        }

        private static string GetCaption(CommandDescription command)
        {
            return $"{(int)command.Type}. {command.Caption}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Saver
{
    internal sealed class DatabaseMatrixLoadQueryBuilder
    {
        public string BuildQuery(string name)
        {
            return "SELECT * FROM " + name + " ORDER BY LINE_ID";
        }
    }
}

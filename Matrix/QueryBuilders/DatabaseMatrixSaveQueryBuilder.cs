﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Saver
{
    internal sealed class DatabaseMatrixSaveQueryBuilder
    {
        public string BuildQuery(IList<MatrixItem> list, string name)
        {
            var sql = CreateTable(name) + CreateInsertRows(list,name);
            return sql;
                      
        }

        private string CreateTable(string name)
        {
           
            return "CREATE TABLE matrix.dbo." + name + "( LINE_ID INT PRIMARY KEY IDENTITY,LINE_VALUE varchar(max)) ";
        }

        private string CreateInsertRows(IList<MatrixItem> list, string name)
        {
            string insert = "INSERT INTO " + name + " (LINE_VALUE) VALUES ";

            foreach(MatrixItem item in list)
            {
                insert += "( '" + item.Rows + "' ),";
            }
            insert = insert.Remove(insert.Length - 1);
            return insert;
        }
    }
}

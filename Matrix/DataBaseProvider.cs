﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Matrixes.Mappers;

namespace Matrixes
{
    internal sealed class DataBaseProvider : IDataBaseProvider
    {
        private readonly IDatabaseConfiguration _configuration;

        public DataBaseProvider(IDatabaseConfiguration databaseConfiguration)
        {
            _configuration = databaseConfiguration ?? throw new ArgumentNullException(nameof(databaseConfiguration));
        }

        public int ExecuteNonQuery(string rawSql)
        {
            using (var connection = new SqlConnection(_configuration.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(rawSql, connection);
                return command.ExecuteNonQuery();
            }
        }

        public IList<T> ExecuteQuery<T>(string rawSql, IMapper<T> mapper)
            where T : class
        {
            using (var connection = new SqlConnection(_configuration.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(rawSql, connection);
                using (var reader = command.ExecuteReader())
                {
                    var items = new List<T>();
                    while (reader.Read())
                    {
                        var item = mapper.Read(reader);
                        items.Add(item);
                    }
                    return items;
                }
            }
        }
    }
}

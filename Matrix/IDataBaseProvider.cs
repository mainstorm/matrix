﻿using System.Collections.Generic;
using Matrixes.Mappers;

namespace Matrixes
{
    public interface IDataBaseProvider
    {
        int ExecuteNonQuery(string rawSql);

        IList<T> ExecuteQuery<T>(string rawSql, IMapper<T> mapper)
            where T : class;
    }
}

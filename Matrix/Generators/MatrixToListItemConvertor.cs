﻿using System;
using System.Collections.Generic;

namespace Matrixes.Generators
{
    public class MatrixToListItemConvertor
    {
        public Matrix ConvertToMatirx(IList<MatrixItem> list)
        {
            int dimension = list.Count;
            var data = new int[dimension, dimension];
            var row = 0;
            foreach (MatrixItem node in list)
            {
                string[] stringSeparators = new string[] { " " };
                string[] columns = node.Rows.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                for (int j = 0; j < dimension; j++)
                {
                    data[row, j] = Convert.ToInt32(columns[j]);
                }
                row++;
            }
            return new Matrix
            {
                Data = data,
                Row = row,
                Column = 0
            };
        }

        public IList<MatrixItem> ConvertToMatrixItems(Matrix matrix)
        {
            var list = new List<MatrixItem>();
            string s = "";

            for (int i = 0; i < matrix.Row; i++)
            {
                for (int j = 0; j < matrix.Column; j++)
                    s += matrix.Data[i, j].ToString() + " ";
                list.Add(new MatrixItem(s));
                s = "";
            }
            return list;
        }
    }
}

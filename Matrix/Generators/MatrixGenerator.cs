﻿using System;

namespace Matrixes.Generators
{
    sealed class MatrixGenerator
    {
        private static Random _random = new Random();

        public Matrix CreateRandomMatrix(int row, int column)
        {
            var data = new int[column, row];
            for (int i = 0; i < column; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    data[i, j] = _random.Next(0, 10);
                }
            }
            return new Matrix
            {
                Row = row,
                Column = column,
                Data = data
            };
        }
    }
}

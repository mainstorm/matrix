﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
namespace Matrixes.Serilization
{
    class BinaryState : ISerialize
    {
        public byte[] Serialize(Matrix matrix)
        {
            var formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, matrix);
                var result = stream.GetBuffer();
                return result;
            }
        }

        public Matrix DeSerialize(byte[] buffer)
        {
            var formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(buffer))
            {
                var result = (Matrix)formatter.Deserialize(stream);
                return result;
            }
        }

    }
}

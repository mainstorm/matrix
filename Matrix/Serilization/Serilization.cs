﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Serilization
{
    sealed class Serialization
    {
        private Matrix _matrix;
        private byte[] _buffer;
        Serialization(Matrix matrix)
        {
            _matrix = matrix;
        }
        Serialization(byte[] buffer)
        {
            _buffer = buffer;
        }
        public byte[] ToByteArray(ISerialize serialize)
        {
            if (_matrix != null)
            {
                serialize.Serialize(_matrix);
            }
            return null;
        }

        public Matrix ToMatrix(ISerialize deserialize)
        {
            if (_buffer != null)
            {
                deserialize.DeSerialize(_buffer);
            }
            return null;
        }
    }
}

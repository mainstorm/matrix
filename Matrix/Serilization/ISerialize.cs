﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Serilization
{
    interface ISerialize
    {
        byte[] Serialize(Matrix matrix);
        Matrix DeSerialize(byte[] buffer);
    }
}

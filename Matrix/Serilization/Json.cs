﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Matrixes.Serilization
{
    class Json : ISerialize
    {
        public byte[] Serialize(Matrix matrix)
        {
            string output = JsonConvert.SerializeObject(matrix);
            var result = Encoding.ASCII.GetBytes(output);
            return result;
        }

        public Matrix DeSerialize(byte[] buffer)
        {
            string input = Encoding.ASCII.GetString(buffer);
            Matrix result = JsonConvert.DeserializeObject<Matrix>(input);
            return result;
        }
    }
}

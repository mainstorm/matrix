﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Matrixes.Generators;
using Newtonsoft.Json;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
namespace Matrixes
{

    class FileProvider 
    {
        private string _path;
        public FileProvider(string nameFile)
        {
            _path = $@"{nameFile}";
        }
        public byte [] Read()
        {
            using (FileStream fstream = new FileStream(_path, FileMode.Open))
            {
                int sizeFile = (int)fstream.Length;//???????
                byte[] fileBuffer = new byte[sizeFile];

                fstream.Read(fileBuffer, 0, sizeFile);
                return fileBuffer;
            }
        }
        public void Write(byte[] buffer)
        {
            using (FileStream fstream = new FileStream(_path, FileMode.OpenOrCreate))
            {
                fstream.Write(buffer, 0, buffer.Length);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Commands
{
    class LoadFromFileCommand
    {
        private readonly SharedCommandData _sharedCommandData;

        public LoadFromFileCommand(SharedCommandData sharedCommandData)
        {
            _sharedCommandData = sharedCommandData ?? throw new ArgumentNullException(nameof(sharedCommandData));
        }

        public void Execute()
        {
            var data = new MatrixFileDao();
            Console.WriteLine("Введите имя файла:");
            var fileName = Console.ReadLine();
            _sharedCommandData.Matrix = data.Load(fileName);
        }
    }
}

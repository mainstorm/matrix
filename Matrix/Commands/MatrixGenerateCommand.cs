﻿using Matrixes.Generators;
using System;

namespace Matrixes.Commands
{
    internal sealed class MatrixGenerateCommand : IApplicationCommand
    {
        private readonly SharedCommandData _sharedCommandData;

        public MatrixGenerateCommand(SharedCommandData sharedCommandData)
        {
            _sharedCommandData = sharedCommandData ?? throw new ArgumentNullException(nameof(sharedCommandData));
        }

        public void Execute()
        {
            var matrixGenerator = new MatrixGenerator();
            _sharedCommandData.Matrix = matrixGenerator.CreateRandomMatrix(3, 3);
        }
    }
}

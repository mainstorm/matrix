﻿namespace Matrixes.Commands
{
    public interface IApplicationCommand
    {
        void Execute();
    }
}

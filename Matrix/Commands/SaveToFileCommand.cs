﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Commands
{
    class SaveToFileCommand
    {
        private readonly SharedCommandData _sharedCommandData;

        public SaveToFileCommand(SharedCommandData sharedCommandData)
        {
            _sharedCommandData = sharedCommandData ?? throw new ArgumentNullException(nameof(sharedCommandData));
        }
        public void Execute()
        {
            var data = new MatrixFileDao();


            if (_sharedCommandData.Matrix != null)
            {
                Console.WriteLine("Введите имя файла:");
                var fileName = Console.ReadLine();
                data.Save(_sharedCommandData.Matrix, fileName);
            }
            else
                Console.WriteLine("Error : Generate matrix.");
        }
    }
}

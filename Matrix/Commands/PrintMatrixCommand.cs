﻿using System;

namespace Matrixes.Commands
{
    internal sealed class PrintMatrixCommand : IApplicationCommand
    {
        private readonly SharedCommandData _sharedCommandData;

        public PrintMatrixCommand(SharedCommandData sharedCommandData)
        {
            _sharedCommandData = sharedCommandData ?? throw new ArgumentNullException(nameof(sharedCommandData));
        }

        public void Execute()
        {
            var matrix = _sharedCommandData.Matrix;
            for (int f = 0; f < matrix.Row; f++)
            {
                for (int k = 0; k < matrix.Column; k++)
                {
                    Console.Write(matrix.Data[f, k] + " ");
                }
                Console.Write('\n');
            }
        }
    }
}

﻿using System.Data;

namespace Matrixes.Mappers
{
    public class MatrixItemMapper : IMapper<MatrixItem>
    {
        public MatrixItem Read(IDataReader dataReader)
        {
            var matrix = new MatrixItem();
            var rowsIndex = dataReader.GetOrdinal("LINE_VALUE");
            matrix.Index  = rowsIndex;
            matrix.Rows   =  (string)dataReader[rowsIndex];
            return matrix;
        }
    }
}

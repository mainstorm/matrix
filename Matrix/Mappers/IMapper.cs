﻿using System.Data;

namespace Matrixes.Mappers
{
    public interface IMapper<T>
    {
        T Read(IDataReader dataReader);
    }
}

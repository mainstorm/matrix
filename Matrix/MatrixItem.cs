﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes
{
    public class MatrixItem
    {
        public int Index { get; set; }

        public string Rows { get; set; }

        public MatrixItem() { }
        public MatrixItem(string row)
        {
            Rows = row;
        }
    }
}

﻿using System;

namespace Matrixes
{
    [Serializable]
    public sealed class Matrix
    {
        public int[,] Data { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }
    }
}

﻿namespace Matrixes
{
    public enum CommandType
    {
        Unknown = 0,
        Matrix,
        SaveToDatabase,
        SaveToFile,
        LoadFromFile,
        LoadFromDatabase,
        PrintMatrix,
        Exit
    }
}
